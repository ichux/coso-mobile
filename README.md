# COSO Project - Mobile
This is the mobile part of this COSO project

```commandline
Download https://github.com/expo/create-react-native-app/archive/refs/heads/main.zip

npx create-react-native-app
cd coso-mobile
npm install -g expo-cli
npm run android

npm install
npx expo install react-native-web@~0.18.10 react-dom@18.2.0 @expo/webpack-config@^18.0.1
yarn add react-native-elements react-navigation/native react-native react-native-gesture-handler react-native-reanimated react-native-safe-area-context react-native-safe-area-context
npm start OR yarn start
```

# Detailed Installation Guide
```commandline
npx create-react-native-app
Need to install the following packages:
  create-react-native-app@3.8.0
Ok to proceed? (y) y
✔ What is your app named? … coso-mobile
✔ How would you like to start › Default new app
✔ Downloaded and extracted project files.

Using npm to install packages.

✖ Something when wrong installing JavaScript dependencies. Check your npm logs. Continuing to initialize the app.
✔ Installed CocoaPods CLI
⚠️  Something went wrong running `pod install` in the `ios` directory. Continuing with initializing the project, you can debug this afterwards.

✅ Your project is ready!

To run your project, navigate to the directory and run one of the following npm commands.

- cd coso-mobile
- npm run android
- npm run ios
- npm run web

⚠️  Before running your app on iOS, make sure you have CocoaPods installed and initialize the project:

  cd coso-mobile/ios
  npx pod-install

npm notice
npm notice New patch version of npm available! 9.6.4 -> 9.6.5
npm notice Changelog: https://github.com/npm/cli/releases/tag/v9.6.5
npm notice Run npm install -g npm@9.6.5 to update!

```